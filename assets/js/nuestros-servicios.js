// CÓDIGO PARA EL CHOOSER

$('.floating #showcursos').click(function() {
	$('#resizer').removeClass('col-md-12').addClass('col-md-3');
	$('#resizer').removeClass('floating');
	$('#showcursos').removeClass('col-md-4').addClass('col-md-12');
	$('#showtraduccion, #showaudiovisuales').hide();
	$('#viewcursos').show().css('opacity', '1');
	$('.goback').show();
});

$('.floating #showtraduccion').click(function() {
	$('#resizer').removeClass('col-md-12').addClass('col-md-3');
	$('#resizer').removeClass('floating');
	$('#showtraduccion').removeClass('col-md-4').addClass('col-md-12');
	$('#showcursos, #showaudiovisuales').hide();
	$('#viewtraduccion').show().css('opacity', '1');
	$('.goback').show();
});

$('.floating #showaudiovisuales').click(function() {
	$('#resizer').removeClass('col-md-12').addClass('col-md-3');
	$('#resizer').removeClass('floating');
	$('#showaudiovisuales').removeClass('col-md-4').addClass('col-md-12');
	$('#showcursos, #showtraduccion').hide();
	$('#viewaudiovisuales').show().css('opacity', '1');
	$('.goback').show();
});

$('.gobackbtn').click(function() {
	$('#resizer').toggleClass('col-md-3 col-md-12');
	$('#resizer').addClass('floating');
	$('#viewcursos, #viewaudiovisuales, #viewtraduccion').hide();
	$('#showcursos, #showaudiovisuales, #showtraduccion').removeClass('col-md-12').addClass('col-md-4').show();
	$('.goback').hide();
});